﻿using System;

namespace task_02
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Title title = new Title("The raven");
            Author author = new Author("Poe");
            Content content = new Content("Nevermore!");

            Book book = new Book(title, author, content);

            book.Show();

            Console.ReadKey();
        }
    }
}
