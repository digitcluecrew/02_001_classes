﻿using System;
namespace task_02
{
	class Book
	{
		Title title;
		Author author;
		Content content;

		public Book(Title title, Author author, Content content)
		{
			this.title = title;
			this.author = author;
			this.content = content;
		}

		public void Show()
		{
			title.Show();
			author.Show();
			content.Show();
		}
	}

	class Title
	{
		string content;

		public Title(string content)
		{
			this.content = content;
		}

		public void Show()
		{
			ConsoleColor color = Console.ForegroundColor;

			Console.ForegroundColor = ConsoleColor.Red;

			Console.WriteLine(this.content);

			Console.ForegroundColor = color;
		}
	}

	class Author
	{
		string content;

		public Author(string content)
		{
			this.content = content;
		}

		public void Show()
		{
			ConsoleColor color = Console.ForegroundColor;

			Console.ForegroundColor = ConsoleColor.Cyan;

			Console.WriteLine(this.content);

			Console.ForegroundColor = color;
		}
	}

	class Content
	{
		string content;

		public Content(string content)
		{
			this.content = content;
		}

		public void Show()
		{
			ConsoleColor color = Console.ForegroundColor;

			Console.ForegroundColor = ConsoleColor.DarkGreen;

			Console.WriteLine(this.content);

			Console.ForegroundColor = color;
		}
	}
}
