﻿using System;

namespace task_00
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Address myAdrs = new Address();

            myAdrs.Index = 100500;
            myAdrs.Country = "Lalaland";
            myAdrs.City = "Somwhere city";
            myAdrs.Street = "The Main street";
            myAdrs.House = 665;
            myAdrs.Apartment = 12;

            Console.WriteLine("Index: {0}", myAdrs.Index);
            Console.WriteLine("Country: {0}", myAdrs.Country);
            Console.WriteLine("City: {0}", myAdrs.City);
            Console.WriteLine("Street: {0}", myAdrs.Street);
            Console.WriteLine("House: {0}", myAdrs.House);
            Console.WriteLine("Apartment: {0}", myAdrs.Apartment);
            Console.ReadKey();
        }
    }

    class Address
    {
        int index;
        string country;
        string city;
        string street;
        int house;
        int apartment;

        public int Index
        {
            get { return this.index; }
            set { this.index = value; }
        }

        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }

        public string City {
            get { return this.city; }
            set { this.city = value; }
        }

        public string Street {
            get { return this.street; }
            set { this.street = value; }
        }

        public int House {
            get { return this.house; }
            set { this.house = value; }
        }

        public int Apartment {
            get { return this.apartment; }
            set { this.apartment = value; }
        }
    }
}
