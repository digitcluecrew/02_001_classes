﻿using System;

namespace task_01
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Side 1:");
            double side1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Side 2:");
            double side2 = double.Parse(Console.ReadLine());

            Rectangle rect = new Rectangle(side1, side2);

            Console.WriteLine("Area: {0}", rect.Area);
            Console.WriteLine("Perimeter: {0}", rect.Perimeter);

            Console.ReadKey();
        }
    }

    class Rectangle
    {
        internal double side1;
        internal double side2;

        public Rectangle(double side1, double side2)
        {
            this.side1 = side1;
            this.side2 = side2;
        }

        public double Area
        {
            get { return this.AreaCalculator(); }
        }

        public double Perimeter
        {
            get { return this.PerimeterCalculator(); }
        }

        double AreaCalculator()
        {
            return this.side1 * this.side2;
        }

        double PerimeterCalculator()
        {
            return 2 * (this.side1 + this.side2);
        }
    }
}
