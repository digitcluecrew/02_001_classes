﻿namespace task_03
{
    class Point
    {
        int x;
        int y;
        string label;

        public Point(int x, int y, string label)
        {
            this.x = x;
            this.y = y;
            this.label = label;
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public string Label
        {
            get { return label; }
        }
    }
}
