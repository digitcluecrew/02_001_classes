﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03
{
    class Figure
    {
        Point[] points;

        public Figure(Point first, Point second, Point third)
        {
            this.points = new Point[] { first, second, third };
        }

        public Figure(Point first, Point second, Point third, Point fourth)
        {
            this.points = new Point[] { first, second, third, fourth };
        }

        public Figure(Point first, Point second, Point third, Point fourth, Point fifth)
        {
            this.points = new Point[] { first, second, third, fourth, fifth };
        }

        double LengthSide(Point A, Point B)
        {
            return Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
        }

        public void PerimeterCalculator()
        {
            double perimeter = LengthSide(points[0], points[points.Length - 1]);

            for (int i = 0; i < points.Length - 1; i++)
            {
                perimeter += LengthSide(points[i], points[i + 1]);
            }

            Console.Write("Perimeter of figure ");
            for (int i = 0; i < points.Length; i++)
            {
                Console.Write(points[i].Label);
            }
            Console.Write(" is {0}", perimeter);
        }
    }
}
