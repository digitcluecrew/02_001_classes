﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(-1, 1, "A");
            Point b = new Point(1, 1, "B");
            Point c = new Point(1, -1, "C");
            Point d = new Point(-1, -1, "D");

            Figure foo = new Figure(a, b, c, d);

            foo.PerimeterCalculator();

            Console.ReadKey();
        }
    }
}
